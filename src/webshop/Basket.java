package webshop;

/*
 * Создать класс Basket, содержащий массив купленных товаров.
 */

import java.util.ArrayList;

public class Basket {

    private ArrayList<Product> purchasedProducts;
    private int purchProdCount;

    public Basket(Product... purchProducts) {

        setPurchProd(purchProducts);
    }

    public Basket(int purchProdCount) {

        purchasedProducts.ensureCapacity(purchProdCount);
        this.purchProdCount = purchProdCount;
    }

    public ArrayList<Product> getPurchasedProductsList() {
        return purchasedProducts;
    }

    public Product[] getPurchasedProductsProductsArray() {
        return (Product[]) purchasedProducts.toArray();
    }


    public void setPurchProd(Product... purchProducts) {

        for (Product pr : purchProducts) {
            purchasedProducts.add(pr);
        }
        purchProdCount = purchasedProducts.size();
    }

    public int getPurchProdCount() {
        return purchProdCount;
    }

    public void setPurchProdCount(int purchProdCount) {
        this.purchProdCount = purchProdCount;
    }
}
