package webshop;

/*
 * Создать класс Категория, имеющий переменные имя и массив товаров.
 */

import java.util.*;

public class ProdCategory {

    private String categoryName;
    private ArrayList<Product> productsArray;
    private int categProdCount;

    public ProdCategory(String categName, int categProdCount) {

        this.categoryName = categName;
        this.categProdCount = categProdCount;
        productsArray.ensureCapacity(categProdCount);

    }

    public ProdCategory(String categName, Product... products) {

        this.categoryName = categName;
        //setProducts(products);
    }

    /*public ProdCategory() {

    }*/

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<Product> getProductsList() {
        return productsArray;
    }

    public Product[] getProductsArray() {
        return (Product[]) productsArray.toArray();
    }


    public void setProducts(Product... products) {

        if (products.length == 0) {
            System.out.println("There should be some parameters in this function");
        } else {

            for (Product pr : products) {
                if (products.length > 0 && productsArray.isEmpty() && pr.getProdCategory().equals(categoryName) && !productsArray.contains(pr)) {
                    productsArray.add(pr);
                }
                else if (!productsArray.isEmpty()) {

                }
        }
         //else if (products.length == 0


        }
        categProdCount =productsArray.size();
    }

    public int getCategProdCount() {
        return categProdCount;
    }

    public void setCategProdCount(int categProdCount) {
        this.categProdCount = categProdCount;
    }


    @Override
    public String toString() {
        return "ProdCategory{" +
                "categName='" + categoryName + '\'' +
                ", products=" + productsArray.toString() +
                ", categProdCount=" + categProdCount +
                '}';
    }
}
